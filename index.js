const fetch = require('node-fetch');
const cheerio = require('cheerio');
const products = require('./product.json');
const CronJob = require('cron').CronJob;
const Telegraf = require('telegraf');
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0

const token = '613925338:AAH5jLls-uj56VB3ZtcTExeuVc47-bqAEr0';
const app = new Telegraf(token);


const getDetailroduct = (url) => new Promise((resolve, reject) => {
    fetch(url, {
        method: 'GET',
        headers: {
            'Referer': 'https://sepatucompass.com/',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36',
        }
    })
    .then(res => res.text())
    .then(async res => {
        const $ = await cheerio.load(res);
        const newProd = [];
        // const prod = await $('button.single_add_to_cart_button.button.alt').text();
        const prod = await $('div').text();
        resolve(prod)
    })
    .catch(err => reject(err))
});

(async () => {
    console.log('');
    console.log('=== STARTING SCRIPT ===');
    console.log('');
    
    const job = new CronJob('*/3 * * * *', async function() {
        const d = new Date();
        console.log('Every Three Minute : ', d);
        for (let index = 0; index < products.length; index++) {
            const prodUrl = products[index];
            const checkStatus = await getDetailroduct(prodUrl);
            if (!checkStatus.includes('out of stock')) {
                app.telegram.sendMessage(285431595,`Add To Cart => ${prodUrl}`) //rubah 285431595 ke chat_id bot yang disediakan
            }else{
                console.log('Tidak ada stock', `=> ${prodUrl}`)
            }

        }
        console.log('');
    });
    job.start();
})();